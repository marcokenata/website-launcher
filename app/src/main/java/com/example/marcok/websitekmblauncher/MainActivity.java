package com.example.marcok.websitekmblauncher;

import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import static android.R.id.progress;
import static com.example.marcok.websitekmblauncher.R.id.progressBar;
import static com.example.marcok.websitekmblauncher.R.id.progress_horizontal;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        final WebView wv = (WebView) findViewById(R.id.webview);
        Button buttonBack = (Button) findViewById(R.id.buttonBack);
        Button buttonForward = (Button) findViewById(R.id.buttonForward);
        Button buttonRefresh = (Button) findViewById(R.id.buttonRefresh);
        final ProgressBar mProgressBar = (ProgressBar) findViewById(progressBar) ;
        mProgressBar.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);


        wv.setWebViewClient(new WebViewClient());
        wv.getSettings().getBuiltInZoomControls();
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("http://kmbui.ui.ac.id/");
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wv.goBack();
            }
        });

        buttonForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wv.goForward();
            }
        });

        buttonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wv.loadUrl("http://kmbui.ui.ac.id");
            }
        });


        wv.setWebViewClient(new WebViewClient() {
                                     @Override public void onPageStarted(WebView view, String url, Bitmap favicon) {
                                         super.onPageStarted(view, url, favicon);
                                         final ProgressBarAnimation anim = new ProgressBarAnimation(mProgressBar,0,progress);
                                         anim.setDuration(1000150);
                                         mProgressBar.startAnimation(anim);
                                         mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                                         wv.setVisibility(View.VISIBLE);
                                         mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                                     }

                                     @Override public void onPageCommitVisible(WebView view, String url) {
                                         super.onPageCommitVisible(view, url);
                                         mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                                         wv.setVisibility(View.VISIBLE);
                                         mProgressBar.setVisibility(ProgressBar.INVISIBLE);

                                     }
                                 });

        wv.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                final String filename = URLUtil.guessFileName(url, contentDisposition, mimetype);
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                Toast.makeText(getApplicationContext(), "Downloading File", Toast.LENGTH_LONG).show();
            }
        });

        wv.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack()) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }
                return false;
            }
        });
            }

    }
